# Profico #

NodeJS Api for fetching Chuck Norris jokes and sends them to users on email.
Currently uses gmail servise for sending emails.

## Dependencies
Express.js, TypeScript, TypeOrm, Swagger, Postgres, Axios, JsonWebToken

## Project setup
Position your self into root of backend folder and type:
```
npm install
```

### Configuration
First create your own .env file in root of backend folder.
For example look at .env.example and by thet example create your own.

Change values of config.json aswell if needed.

### Db creation
npm run schema:sync

### Compiles and hot-reloads for development
```
npm run start:dev
```
### Compiles and minifies for production
```
npm run start
```
### Swagger for testing api
```
apiurl +/docs

For example http://localhost:9000/docs
```
* create user with /signup endpoint
* from signup response take value of token and apply it to the swagger authorization
* call /api/joke to get joke on email