import { BaseApiError } from "src/app/BaseApiError";

export class NotBeforeError extends BaseApiError{
  constructor(description) {
    super("Bad Request", 400, description);
  }
}