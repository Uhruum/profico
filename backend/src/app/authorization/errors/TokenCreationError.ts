import { BaseApiError } from "../../BaseApiError";

export class TokenCreationError extends BaseApiError{
  constructor(description) {
    super("Internal Server Error", 500, description);
  }
}