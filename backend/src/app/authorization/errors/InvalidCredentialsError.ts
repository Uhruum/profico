import { BaseApiError } from "../../BaseApiError";

export class InvalidCredentialsError extends BaseApiError{
  constructor(description) {
    super("Unprocessable Entity", 422, description);
  }
}