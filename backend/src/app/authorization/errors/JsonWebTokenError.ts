import { BaseApiError } from "src/app/BaseApiError";

export class JsonWebTokenError extends BaseApiError{
  constructor(description) {
    super("Bad Request", 400, description);
  }
}