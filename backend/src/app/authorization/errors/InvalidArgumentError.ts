import { BaseApiError } from "../../BaseApiError"

export class InvalidArgumentError extends BaseApiError{
  constructor(description) {
    super("Bad Request", 400, description);
  }
}