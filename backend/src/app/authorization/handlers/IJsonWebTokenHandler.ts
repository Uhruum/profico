import { User } from "../../../database/entity/User";
import { TokenDto } from "../dtos/TokenDto";
import { UserDto } from "../dtos/UserDto";

export interface IJsonWebTokenHandler{
  createToken(user: User): Promise<TokenDto>
  verifyToken(token: string) : Promise<UserDto>;
}