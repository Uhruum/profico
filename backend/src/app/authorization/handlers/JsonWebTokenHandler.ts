import { User } from "../../../database/entity/User";
import { TokenDto } from "../dtos/TokenDto";
import { IJsonWebTokenHandler } from "./IJsonWebTokenHandler";
import jwt from "jsonwebtoken";
import { TokenCreationError } from "../errors/TokenCreationError";
import dotenv from "dotenv";
import { TokenExpiredError } from "../errors/TokenExpiredError";
import { JsonWebTokenError } from "../errors/JsonWebTokenError";
import { NotBeforeError } from "../errors/NotBeforeError";
import { UserDto } from "../dtos/UserDto";

dotenv.config({});

export class JsonWebTokenHandler implements IJsonWebTokenHandler {
  async createToken(user: User): Promise<TokenDto> {
    try {
      const tokenExpire = Number(process.env.TOKEN_EXPIRE);
      const secretKey = process.env.SECRET_KEY;
      const expire = new Date(Date.now() + tokenExpire * 60 * 1000);
      const tokenValue = await jwt.sign(
        { userid: user.id, username: user.email, expire: expire },
        secretKey
      );

      return new TokenDto(tokenValue, expire, user.id);
    } catch (e) {
      throw new TokenCreationError("Could not create jwt token!");
    }
  }
  async verifyToken(token: string): Promise<UserDto> {

    const verifyResult = await jwt.verify(token, process.env.SECRET_KEY , (err, decoded) => {
      if (err) {
        return err;
      }
      return decoded;
    });

    if (verifyResult.name) {
      switch (verifyResult.name) {
        case "JsonWebTokenError":
          throw new JsonWebTokenError(verifyResult.message);
        case "NotBeforeError":
          throw new NotBeforeError(verifyResult.message);
        default:
          throw new TokenExpiredError("Token expired!");
        }
    }
    return new UserDto(verifyResult.userid, verifyResult.username);
  }
}
