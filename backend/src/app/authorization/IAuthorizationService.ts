import { TokenDto } from "./dtos/TokenDto";
import { LoginDto } from "./dtos/LoginDto";
import { SignupDto } from "./dtos/SignupDto";

export interface IAuthorizationService{
  login(loginDto: LoginDto): Promise<TokenDto>;
  signup(signupDto: SignupDto): Promise<TokenDto>;
}