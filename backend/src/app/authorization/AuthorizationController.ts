import { Body, Controller, Post, Route, Tags } from "tsoa";
import { AuthorizationService } from "./AuthorizationService";
import { LoginDto } from "./dtos/LoginDto";
import { SignupDto } from "./dtos/SignupDto";
import { IAuthorizationService } from "./IAuthorizationService";

@Tags('Api authorization')
@Route('/api/auth')
export class AuthorizationController extends Controller{
  private authorizationService: IAuthorizationService = new AuthorizationService();

  @Post('/login')
  public async login(@Body() body:LoginDto) {
      return this.authorizationService.login(body);
  }

  @Post('/signup')
  public async signup(@Body() body:SignupDto) {
      return this.authorizationService.signup(body);
  }
}