export interface IEmailValidation{
  validateEmail(email:string) : boolean
}