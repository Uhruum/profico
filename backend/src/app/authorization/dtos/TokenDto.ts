export class TokenDto{
  token: string;
  expire: Date;
  userId: number;

  public constructor(token: string, expire: Date, userId:number) {
    this.token = token;
    this.expire = expire;
    this.userId = userId;
  }
}