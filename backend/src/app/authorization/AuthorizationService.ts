import { getConnection } from "typeorm";
import { User } from "../../database/entity/User";
import { LoginDto } from "./dtos/LoginDto";
import { SignupDto } from "./dtos/SignupDto";
import { TokenDto } from "./dtos/TokenDto";
import { InvalidCredentialsError } from "./errors/InvalidCredentialsError";
import { IAuthorizationService } from "./IAuthorizationService";
import bcrypt from "bcrypt";
import { Token } from "../../database/entity/Token";
import { JsonWebTokenHandler } from "./handlers/JsonWebTokenHandler";
import { IJsonWebTokenHandler } from "./handlers/IJsonWebTokenHandler";
import { EmailValidation } from "./validation/EmailValidation";
import { IEmailValidation } from "./validation/IEmailValidation";
import { InvalidArgumentError } from "./errors/InvalidArgumentError";

export class AuthorizationService implements IAuthorizationService {
  private jwtHandler: IJsonWebTokenHandler = new JsonWebTokenHandler();
  private emailValidator: IEmailValidation = new EmailValidation();

  async login(loginDto: LoginDto): Promise<TokenDto> {
    if (!this.emailValidator.validateEmail(loginDto.email))
      throw new InvalidCredentialsError("Invalid email or password!");

    const userRepository = getConnection().getRepository(User);
    const user = await userRepository.findOne({ email: loginDto.email });
    if (user === undefined)
      throw new InvalidCredentialsError("Invalid email or password!");
    const passMatch = await bcrypt.compare(loginDto.password, user.password);
    if (!passMatch)
      throw new InvalidCredentialsError("Invalid email or password!");

    const createdTokenDto = await this.jwtHandler.createToken(user);
    const tokenRepository = getConnection().getRepository(Token);
    let token = await tokenRepository.findOne({ user: user });

    if (token === undefined) {
      const newToken = await tokenRepository.save(new Token(createdTokenDto.token,createdTokenDto.expire,user));
      return new TokenDto(newToken.token, newToken.expire, user.id);
    }

    token.expire = createdTokenDto.expire;
    token.token = createdTokenDto.token;
    tokenRepository.save(token);

    return new TokenDto(token.token, token.expire, user.id);
  }
  async signup(signupDto: SignupDto): Promise<TokenDto> {
    if (!signupDto.firstName ||!signupDto.lastName ||!signupDto.email ||!signupDto.password)
      throw new InvalidArgumentError("Al data is required!");

    if (!this.emailValidator.validateEmail(signupDto.email))
      throw new InvalidCredentialsError("Email must be valid!");

    const userRepository = getConnection().getRepository(User);

    const users = await userRepository.find({ email: signupDto.email });
    if (users.length > 0)
      throw new InvalidCredentialsError("User with that email exists!");

    const newUser = await userRepository.save(
      new User(signupDto.firstName,signupDto.lastName,signupDto.email,signupDto.password)
    );
    const createdTokenDto = await this.jwtHandler.createToken(newUser);
    const tokenRepository = getConnection().getRepository(Token);
    const newToken = await tokenRepository.save(new Token(createdTokenDto.token,createdTokenDto.expire,newUser));
    return new TokenDto(newToken.token, newToken.expire, newUser.id);
  }
}
