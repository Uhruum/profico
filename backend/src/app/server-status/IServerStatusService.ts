import { ServerStatusDto } from "./dtos/ServerStatusDto";

export interface IServerStatusService {
  getServerStatus(): Promise<ServerStatusDto>;
}