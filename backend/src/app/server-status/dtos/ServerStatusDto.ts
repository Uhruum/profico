export class ServerStatusDto{
  status: string
  serverTime: string

  public constructor (status:string, serverTime:string) {
    this.status = status;
    this.serverTime = serverTime;
  }
}