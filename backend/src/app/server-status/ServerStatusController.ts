import { Controller, Get, Route, Tags } from "tsoa";
import { IServerStatusService } from "./IServerStatusService";
import { ServerStatusService } from "./ServerStatusService";

@Tags('Server Status')
@Route('/api/server-status')
export class ServerStatusController extends Controller{
  private serverStatusService: IServerStatusService = new ServerStatusService();

  @Get('/')
  public async getServerStatus() {
      return this.serverStatusService.getServerStatus();
  }

}



