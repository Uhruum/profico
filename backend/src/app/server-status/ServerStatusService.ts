import { ServerStatusDto } from "./dtos/ServerStatusDto";
import { IServerStatusService } from "./IServerStatusService";

export class ServerStatusService implements IServerStatusService {
  async getServerStatus(): Promise<ServerStatusDto> {
    return new ServerStatusDto("server is running", new Date().toISOString());
  }
}
