import { User } from "@entity/User";
import { getConnection } from "typeorm";
import { UserNotFoundError } from "./errors/UserNotFoundError";
import { GmailService } from "./GmailService";
import { IcndbApiService } from "./IcndbApiService";
import { IGmailService } from "./IGmailService";
import { IIcndbApiService } from "./IIcndbApiService";
import { IJokeApiService } from "./IJokeApiService";

export class JokeApiService implements IJokeApiService {
  private icndbApiService: IIcndbApiService = new IcndbApiService();
  private gmailService: IGmailService = new GmailService();

  async getRandomJoke( userId:number): Promise<string> {
    
    const userRepository = getConnection().getRepository(User);
    const user = await userRepository.findOne({ where: { id: userId } });

    if (user === undefined)
      throw new UserNotFoundError("Could not find user!");

    const joke = await this.icndbApiService.getRandomJoke();

    const gmailResponse = await this.gmailService.sendMail(
      user.email,
      "Random generated joke",
      joke.value
    );

    return gmailResponse + " Please check your email!";
  }
}
