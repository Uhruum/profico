import { BaseApiError } from "../../BaseApiError";

export class IcndbApiError extends BaseApiError{
  constructor(name,statusCode,description) {
    super(name, statusCode, description);
  }
}