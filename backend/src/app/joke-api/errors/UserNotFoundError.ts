import { BaseApiError } from "../../BaseApiError";

export class UserNotFoundError extends BaseApiError{
  constructor(description) {
    super("Bad Request", 404, description);
  }
}