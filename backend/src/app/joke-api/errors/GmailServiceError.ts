import { BaseApiError } from "../../BaseApiError";

export class GmailServiceError extends BaseApiError{
  constructor() {
    super("Internal Server Error", 500, "Error occured when sending email. Please contact administrator!");
  }
}