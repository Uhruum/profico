export interface IGmailService{
  sendMail(to: string, subject: string, content: string) : Promise<string>;
}