import { JokeApiResponseDto } from "./dtos/JokeApiResponseDto";

export interface IIcndbApiService{
  getRandomJoke(): Promise<JokeApiResponseDto>;
}