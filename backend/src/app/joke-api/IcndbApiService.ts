import axios, { AxiosError } from "axios";
import { JokeApiResponseDto } from "./dtos/JokeApiResponseDto";
import { IcndbApiError } from "./errors/IcndbApiError";
import { IIcndbApiService } from "./IIcndbApiService";
import dotenv from 'dotenv';

dotenv.config({});

export class IcndbApiService implements IIcndbApiService {
  private static jokeAxios = axios.create({});

  async getRandomJoke(): Promise<JokeApiResponseDto> {
    try {
      const url = process.env.ICNDB_API;
      IcndbApiService.jokeAxios.defaults.baseURL = url;
      const response = await IcndbApiService.jokeAxios.get<JokeApiResponseDto>('/jokes/random')
      return response.data;
    } catch (e) {
      const error = e as AxiosError;
      throw new IcndbApiError(error.name, 500, error.message);
    }
  }
}
