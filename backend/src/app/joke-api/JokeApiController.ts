import { Controller, Get, Route, Tags } from "@tsoa/runtime";
import { Hidden, Query } from "tsoa";
import { IJokeApiService } from "./IJokeApiService";
import { JokeApiService } from "./JokeApiService";

@Tags('Joke api')
@Route('/api/joke')
export class JokeApiController extends Controller{
  private jokeApiService: IJokeApiService = new JokeApiService();

  @Get('/')
  public async getRandomJoke(@Query() @Hidden() userId: number = 0) {
      return this.jokeApiService.getRandomJoke(userId);
  }
}
