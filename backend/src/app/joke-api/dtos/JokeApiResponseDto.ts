export class JokeApiResponseDto{
  created_at: Date
  id: number
  updated_at: Date
  url: string
  value: string
}