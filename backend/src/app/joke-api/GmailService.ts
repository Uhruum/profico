import * as nodemailer from "nodemailer";
import { IGmailService } from "./IGmailService";
import dotenv from "dotenv";
import { GmailServiceError } from "./errors/GmailServiceError";

dotenv.config({});
export class GmailService implements IGmailService {
  private _transporter: nodemailer.Transporter;

  constructor() {
    this._transporter = nodemailer.createTransport({
      service: "gmail",
      auth: {
        user: process.env.GMAIL_USERNAME,
        pass: process.env.GMAIL_APP_PASSWORD,
      },
    });
  }

  async sendMail(to: string, subject: string, content: string): Promise<string> {

    let options = {
      from: process.env.GMAIL_USERNAME,
      to: to,
      subject: subject,
      html: "<p>" + content + "</p>",
    };

    try {
      await this._transporter.sendMail(options);
      return "Joke sent!";
    } catch {
      throw new GmailServiceError();
    }
  }
}
