export interface IJokeApiService{
  getRandomJoke( userId:number): Promise<string>;
}