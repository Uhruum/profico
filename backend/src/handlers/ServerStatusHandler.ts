import { ServerStatusController } from '../app/server-status/ServerStatusController';
export class ServerStatusHandler{
  async getServerStatus(req, res, next) {
      
    const controller = new ServerStatusController();

    const response = await controller.getServerStatus();
    return res.json(response);
  }
}

