import { AuthorizationController } from "../app/authorization/AuthorizationController";
import { BaseApiError } from "../app/BaseApiError";

export class AuthorizationHandler {
  async login(req, res, next) {
    try {
      const authorizationController = new AuthorizationController();
      const response = await authorizationController.login(req.body);
      return res.json(response);
    } catch (error) {
      if (error instanceof BaseApiError) {
        return res.status(error.httpCode).json({
          message: error.message,
        });
      }
      next(error);
    }
  }

  async signup(req, res, next) {
    try {
      const authorizationController = new AuthorizationController();
      const response = await authorizationController.signup(req.body);
      return res.json(response);
    } catch (error) {
      if (error instanceof BaseApiError) {
        return res.status(error.httpCode).json({
          message: error.message,
        });
      }
      next(error);
    }
  }
}
