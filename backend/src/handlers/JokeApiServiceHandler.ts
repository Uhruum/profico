import { JsonWebTokenHandler } from "src/app/authorization/handlers/JsonWebTokenHandler";
import { BaseApiError } from "../app/BaseApiError";
import { JokeApiController } from "../app/joke-api/JokeApiController";

export class JokeApiServiceHandler {

  async getRandomJoke(req, res, next) {
    try {
      const bearerHeader = req.headers["authorization"];
    
      if (bearerHeader !== undefined) {
        const bearer = bearerHeader.split(" ");
        const bearerToken = bearer[1];

        const jwtHandler= new JsonWebTokenHandler();
        const user = await jwtHandler.verifyToken(bearerToken);
        const jokeApiController = new JokeApiController();
        const response = await jokeApiController.getRandomJoke(user.id);
        return res.json(response);
      }
      res.sendStatus(403);
    } catch (error) {
      if (error instanceof BaseApiError) {
        return res.status(error.httpCode).json({
          message: error.message,
        });
      }
      next(error);
    }
  }
}
