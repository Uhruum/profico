import express, { Express } from "express";
import morgan from "morgan";
import cors from "cors";
import swaggerUi from "swagger-ui-express";
import serverStatusRouter from "./routers/serverStatus.router";
import authorizationRouter from "./routers/authorization.router";
import jokeRouter from "./routers/jokeApiService.router";
import "../src/database/index";
import { BaseApiError } from "./app/BaseApiError";
const app: Express = express();

/************************************************************************************
 *                              Basic Express Middlewares
 ***********************************************************************************/

app.set("json spaces", 4);
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use(morgan("dev"));
app.use(cors());

/************************************************************************************
 *                               Register all routes
 ***********************************************************************************/
app.use("/api/server-status", serverStatusRouter);
app.use("/api/auth", authorizationRouter);
app.use("/api/joke", jokeRouter);

app.use(
  "/docs",
  swaggerUi.serve,
  async (req: express.Request, res: express.Response) => {
    return res.send(
      swaggerUi.generateHTML(await import("../tsoa/swagger.json"))
    );
  }
);

/************************************************************************************
 *                               Express Error Handling
 ***********************************************************************************/

// eslint-disable-next-line @typescript-eslint/no-unused-vars
app.use(
  (
    err: Error,
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) => {
    if (err instanceof BaseApiError) {
      return res.status(err.httpCode).json({
        message: err.message,
      });
    }
    if (err instanceof Error) {
      return res.status(500).json({
        errorName: err.name,
        message: err.message,
        stack: err.stack || "no stack defined",
      });
    }
    next(err);
  }
);
app.use(function notFoundHandler(_req, res: express.Response) {
  return res.status(404).send({ message: "Not Found" });
});
export default app;
