import express from 'express';
import { ServerStatusHandler } from '../handlers/ServerStatusHandler';

const serverStatusHandler = new ServerStatusHandler();
const router = express.Router();
router.route('/').get(serverStatusHandler.getServerStatus);


export default router;