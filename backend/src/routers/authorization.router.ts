import express from "express";
import { AuthorizationHandler } from "../handlers/AuthorizationHandler";
const authorizationHandler = new AuthorizationHandler();
const router = express.Router();
router.route('/login').post(authorizationHandler.login);
router.route('/signup').post(authorizationHandler.signup);
export default router;