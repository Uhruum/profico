import express from "express";
import { JokeApiServiceHandler } from "../handlers/JokeApiServiceHandler";
export const methodNotAllowed = (req, res, next) => res.sendStatus(405);
const jokeApiServiceHandler = new JokeApiServiceHandler();
const router = express.Router();
router.route('/').get(jokeApiServiceHandler.getRandomJoke).all(methodNotAllowed);
export default router;