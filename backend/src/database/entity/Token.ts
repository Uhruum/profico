import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  BaseEntity,
  ManyToOne,
  JoinColumn,
} from "typeorm";
import { User } from "./User";

@Entity()
export class Token extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  token: string;

  @Column()
  expire: Date;

  @ManyToOne(() => User)
  @JoinColumn()
  user: User;

  public constructor(token: string, expire: Date, user: User) {
    super();
    this.token = token;
    this.expire = expire;
    this.user = user;
  }
}
