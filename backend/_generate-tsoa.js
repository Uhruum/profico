const { generateRoutes, generateSpec } = require('tsoa');

(async () => {
  const specOptions = {
    basePath: "/",
    entryFile: "src/server.ts",
    noImplicitAdditionalProperties: "throw-on-extras",
    controllerPathGlobs: ["src/**/*Controller.ts"],
    outputDirectory: "tsoa",
    specVersion: 3
  };

  const routeOptions = {
    middleware: "express",
    basePath: "/",
    entryFile: "src/server.ts",
    noImplicitAdditionalProperties: "throw-on-extras",
    controllerPathGlobs: ["src/**/*Controller.ts"],
    routesDir: "tsoa",
  };

  await generateSpec(specOptions);
  await generateRoutes(routeOptions);
})();